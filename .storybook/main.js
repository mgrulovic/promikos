const path = require('path');
const fs = require('fs');

module.exports = {
  stories: ['../stories/**/*.stories.js'],
  addons: ['@storybook/addon-actions', '@storybook/addon-links'],
  webpackFinal: async (config, { configType }) => {
    config.module.rules.push({
      test: /\.scss$/,
      use: [
        'style-loader', // creates style nodes from JS strings
        'css-loader', // translates CSS into CommonJS
        {
          loader: 'sass-loader',
          options: {
            data: `
						@import "@/assets/scss/main.scss";
					`,
          },
        },
      ],

      include: path.resolve(__dirname, '../'),
    });
    config.resolve.alias['@'] = path.resolve('src');

    // Return the altered config
    return { ...config, node: { fs: 'empty' } };
  },
};
