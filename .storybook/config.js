import Vue from 'vue';
import { configure } from '@storybook/vue';

import validation from '../src/services/validation';

// automatically import all files ending in *.stories.js
const req = require.context('../stories', true, /.stories.js$/);
function loadStories() {
  req.keys().forEach(filename => req(filename));
}
Vue.component('ValidationProvider', validation.ValidationProvider);
Vue.component('ValidationObserver', validation.ValidationObserver);
Vue.prototype.$t = (key) => key;
Vue.prototype.$route = { name: 'Active' };
configure(loadStories, module);