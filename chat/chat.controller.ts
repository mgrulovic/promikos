import {
  ApiTags,
  ApiBearerAuth,
  ApiUnauthorizedResponse,
  ApiForbiddenResponse,
} from '@nestjs/swagger';
import {
  Controller,
  UseGuards,
  Put,
  BadRequestException,
  ForbiddenException,
  Param,
  ParseUUIDPipe,
  NotFoundException,
  Get,
} from '@nestjs/common';
import { JwtAuthGuard } from '../../core/guards/jwt-auth.guard';
import { ChatService } from './chat.service';
import { GetUser } from '../../core/decorators/get-user.decorator';
import { User } from '../../database/entities/user.entity';
import { ChatResDto } from './dtos/res/chat.dto';
import { isErr } from '../../core/errors/result';
import {
  InvalidCorrespondentUser,
} from './chat.errors';
import { ForbiddenOperation } from '../common.errors';
import { UserNotFound } from '../user/user.errors';

@ApiTags('Chat')
@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
@ApiUnauthorizedResponse({ description: 'Unauthorized' })
@ApiForbiddenResponse({ description: 'Forbidden' })
@Controller('/chats')
export class ChatController {
  constructor(private chatService: ChatService) {}

  @Put('/:userId/:correspondentId')
  async getOrCreate(
    @Param('userId', ParseUUIDPipe) userId: string,
    @Param('correspondentId', ParseUUIDPipe) correspondentId: string,
    @GetUser() currentUser: User,
  ): Promise<ChatResDto> {
    const result = await this.chatService.findOrCreate(
      currentUser.id,
      userId,
      correspondentId,
    );
    if (isErr(result)) {
      switch (result.error.code) {
        case ForbiddenOperation().code:
          throw new ForbiddenException(result.error.message);
        case UserNotFound().code:
          throw new NotFoundException(result.error.message);
        case InvalidCorrespondentUser().code:
          throw new BadRequestException(result.error.message);
      }
    }
    return new ChatResDto(result.value);
  }

  @Get('/:userId')
  async get(
    @Param('userId', ParseUUIDPipe) userId: string,
    @GetUser() currentUser: User,
  ): Promise<ChatResDto[]> {
    const result = await this.chatService.getAllChats(
      currentUser.id,
      userId,
    );
    if (isErr(result)) {
      switch (result.error.code) {
        case ForbiddenOperation().code:
          throw new ForbiddenException(result.error.message);
        case UserNotFound().code:
          throw new NotFoundException(result.error.message);
      }
    }
    return result.value.map(chat => new ChatResDto(chat));
  }
}
