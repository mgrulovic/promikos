import {
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  OnGatewayConnection,
  OnGatewayDisconnect,
  ConnectedSocket,
  MessageBody,
} from '@nestjs/websockets';
import { Logger } from '@nestjs/common';
import { Socket, Server } from 'socket.io';
import { AuthService } from '../auth/auth.service';
import { InjectRepository } from '@nestjs/typeorm';
import { MessageRepository } from '../../database/repositories/message.repository';
import { MessageResDto } from './dtos/res/message.dto';
import { GameNotificationService } from '../game/game-notification.service';

interface IUserSocket {
  id: string;
  socket: Socket;
}

interface INewMessage {
  chatId: string;
  text: string;
  sentAt: Date;
  correspondent: {
    id: string;
  };
  sender: {
    id: string;
    fullName: string;
  };
}

@WebSocketGateway({ namespace: 'chat' })
export class ChatGateway
  implements OnGatewayConnection, OnGatewayDisconnect {
  private logger: Logger = new Logger('ChatGateway');
  private connectedUsers: IUserSocket[] = [];

  @WebSocketServer()
  server: Server;

  constructor(
    @InjectRepository(MessageRepository)
    private messageRepository: MessageRepository,

    private authService: AuthService,
    private gameNotificationService: GameNotificationService,
  ) {}

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async handleConnection(socket: Socket, ...args: any[]) {
    const jwt: string = socket.handshake.query.jwt;
    const user = await this.authService.validateWsAuth(jwt);
    // check if user is already connected
    const foundConnection = this.connectedUsers.find(
      connectedUser => connectedUser.id === user.id,
    );
    if (foundConnection) {
      foundConnection.socket.disconnect(true);
    }
    // add connection
    this.connectedUsers = [...this.connectedUsers, { id: user.id, socket }];
  }

  handleDisconnect(socket: Socket) {
    this.logger.log(`Client disconnected: ${socket.id}`);
    this.connectedUsers = this.connectedUsers.filter(
      connectedUser => connectedUser.socket.id !== socket.id,
    );
  }

  @SubscribeMessage('joinChat')
  async handleJoinChat(
    @ConnectedSocket() clientSocket: Socket,
    @MessageBody() chatId: string,
  ) {
    clientSocket.leaveAll();
    clientSocket.join(chatId);
    return {
      joined: true,
    };
  }

  @SubscribeMessage('message')
  async handleMessage(
    @ConnectedSocket() clientSocket: Socket,
    @MessageBody() messagePayload: INewMessage,
  ) {
    const createdMessage = await this.messageRepository
      .create({
        text: messagePayload.text,
        senderId: messagePayload.sender.id,
        chatId: messagePayload.chatId,
        sentAt: messagePayload.sentAt,
      })
      .save();
    const message = await this.messageRepository.findOne({
      where: { id: createdMessage.id },
      relations: ['sender'],
    });
    this.gameNotificationService.sendChatNotification({
      chatId: message.chatId,
      correspondentId: messagePayload.correspondent.id,
      sender: message.sender,
      textMessage: message.text,
    });
    clientSocket.broadcast.to(message.chatId).emit('message', {
      message: new MessageResDto(message),
      chatId: message.chatId,
    });
  }

  @SubscribeMessage('leaveAllChats')
  handleLeaveAllChat(@ConnectedSocket() clientSocket: Socket) {
    clientSocket.leaveAll();
  }
}
