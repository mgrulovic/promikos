import { User } from '../../../../database/entities/user.entity';

export class SenderResDto {
  id: string;
  full_name: string;

  constructor(sender: User) {
    this.id = sender.id;
    this.full_name = sender.getFullName();
  }
}
