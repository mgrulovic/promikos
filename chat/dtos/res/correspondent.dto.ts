import { ICorrespondent } from '../../interfaces/chat.interface';

export class CorrespondentResDto {
  id: string;
  full_name: string;
  itn: number;

  constructor(correspondent: ICorrespondent) {
    this.id = correspondent.id;
    this.full_name = correspondent.fullName;
    this.itn = correspondent.itn;
  }
}
