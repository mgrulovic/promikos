import { IChat } from '../../interfaces/chat.interface';
import { CorrespondentResDto } from './correspondent.dto';
import { MessageResDto } from './message.dto';

export class ChatResDto {
  id: string;
  correspondent: CorrespondentResDto;
  messages: MessageResDto[];

  constructor(chat: IChat) {
    this.id = chat.id;
    this.correspondent = new CorrespondentResDto(chat.correspondent);
    this.messages = chat.messages.map(
      message => new MessageResDto(message),
    );
  }
}
