import { Message } from '../../../../database/entities/message.entity';
import { SenderResDto } from './sender.dto';

export class MessageResDto {
  id: string;
  content: string;
  sender: SenderResDto;
  created_at: Date;

  constructor(message: Pick<Message, 'id' | 'text' | 'createdAt' | 'sender'>) {
    this.id = message.id;
    this.content = message.text;
    this.sender = new SenderResDto(message.sender);
    this.created_at = message.createdAt;
  }
}
