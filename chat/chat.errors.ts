import { ErrorObject } from 'src/core/errors/result';

export type InvalidCorrespondentUser = ErrorObject<'InvalidCorrespondentUser'>;
export function InvalidCorrespondentUser(): InvalidCorrespondentUser {
  return {
    code: 'InvalidCorrespondentUser',
    message: `Invalid correspondent user`,
  };
}

export type MessageNotFound = ErrorObject<'MessageNotFound'>;
export function MessageNotFound(messageId?: string): MessageNotFound {
  return {
    code: 'MessageNotFound',
    message: `Message with id: ${messageId} is not found.`,
  };
}
