import { Module } from '@nestjs/common';
import { ChatRepository } from '../../database/repositories/chat.repository';
import { MessageRepository } from '../../database/repositories/message.repository';
import { ChatController } from './chat.controller';
import { ChatService } from '../chat/chat.service';
import { ChatGateway } from './chat.gateway';
import { GameNotificationService } from '../game/game-notification.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '../../config/config.module';
import { AuthModule } from '../auth/auth.module';
import { NotificationModule } from '../../notification/notification.module';
import { UserRepository } from '../../database/repositories/user.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([ChatRepository, MessageRepository, UserRepository]),
    ConfigModule,
    AuthModule,
    NotificationModule,
  ],
  controllers: [ChatController],
  providers: [ChatService, ChatGateway, GameNotificationService],
  exports: [ChatService],
})
export class ChatModule {}
