import { Socket } from 'socket.io';

export default interface IUserSocket {
  id: string;
  socket: Socket;
}
