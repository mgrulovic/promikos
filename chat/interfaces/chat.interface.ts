import { Message } from '../../../database/entities/message.entity';

export interface ICorrespondent {
  id: string;
  fullName: string;
  itn: number;
}

export interface IChat {
  id: string;
  correspondent: ICorrespondent;
  messages: Pick<Message, 'id' | 'text' | 'sender' | 'createdAt'>[];
}
