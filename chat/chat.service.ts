import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  AsyncResult,
  Error,
  Success,
  isErr,
} from '../../core/errors/result';
import { MessageRepository } from '../../database/repositories/message.repository';
import { IChat } from './interfaces/chat.interface';
import { User } from '../../database/entities/user.entity';
import {
  InvalidCorrespondentUser,
} from './chat.errors';
import { ForbiddenOperation } from '../common.errors';
import { ChatRepository } from '../../database/repositories/chat.repository';
import { UserRepository } from '../../database/repositories/user.repository';
import { UserNotFound } from '../user/user.errors';
import { Chat } from 'src/database/entities/chat.entity';

@Injectable()
export class ChatService {
  constructor(
    @InjectRepository(ChatRepository)
    private chatRepository: ChatRepository,
    @InjectRepository(MessageRepository)
    private messageRepository: MessageRepository,
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
  ) {}

  async findOrCreate(
    currentUserId: string,
    userId: string,
    correspondentId: string,
  ): AsyncResult<
    IChat,
    ForbiddenOperation | UserNotFound | InvalidCorrespondentUser
  > {
    const validationResult = await this.validateChatPayload(
      currentUserId,
      userId,
      correspondentId,
    );
    if (isErr(validationResult)) {
      return validationResult;
    }
    const { user, correspondent } = validationResult.value;
    let chat = await this.chatRepository.findOne({
      where: [
        {
          creatorId: user.id,
          participantId: correspondent.id,
        },
        {
          creatorId: correspondent.id,
          participantId: user.id,
        },
      ],
    });
    if (!chat) {
      chat = await this.chatRepository
        .create({
          creatorId: user.id,
          participantId: correspondent.id,
        })
        .save();
    }
    const messages = await this.messageRepository.find({
      where: { chatId: chat.id },
      order: { createdAt: 'DESC' },
      relations: ['sender'],
    });
    return Success({
      id: chat.id,
      correspondent: {
        id: correspondent.id,
        fullName: correspondent.getFullName(),
        itn: correspondent.itn,
      },
      messages: messages.map(msg => ({
        id: msg.id,
        text: msg.text,
        sender: msg.sender,
        createdAt: msg.createdAt,
      })),
    });
  }

  async validateChatPayload(
    currentUserId: string,
    userId: string,
    correspondentId: string,
  ): AsyncResult<
    { user: User; correspondent: User },
    ForbiddenOperation | UserNotFound | InvalidCorrespondentUser
  > {
    if (currentUserId !== userId) {
      return Error(ForbiddenOperation());
    }
    const user = await this.userRepository.findOne(userId);
    if (!user) {
      return Error(UserNotFound(userId));
    }

    const correspondent = await this.userRepository.findOne(correspondentId);
    if (!correspondent) {
      return Error(UserNotFound(correspondentId));
    }
    if (user.id === correspondent.id) {
      return Error(InvalidCorrespondentUser());
    }
    return Success({ user, correspondent });
  }

  async getAllChats(
    userId: string,
    currentUserId: string,
  ): AsyncResult<IChat[], ForbiddenOperation | UserNotFound> {
    if (currentUserId !== userId) {
      return Error(ForbiddenOperation());
    }
    const user = await this.userRepository.findOne(userId);
    if (!user) {
      return Error(UserNotFound(userId));
    }

    const chats = await this.chatRepository.find({
      where: [
        {
          creatorId: userId,
        },
        {
          participantId: userId,
        },
      ],
      relations: ['messages', 'participant', 'creator', 'messages.sender'],
    });
    const result = [];
    for (let i = 0; i < chats.length; i += 1) {
      const correspondent =
        chats[i].creator.id !== userId
          ? {
              id: chats[i].creator.id,
              fullName: chats[i].creator.getFullName(),
              itn: chats[i].creator.itn,
            }
          : {
              id: chats[i].participant.id,
              fullName: chats[i].participant.getFullName(),
              itn: chats[i].participant.itn,
            };
      result.push({
        id: chats[i].id,
        correspondent,
        messages: chats[i].messages.map(msg => ({
          id: msg.id,
          text: msg.text,
          sender: msg.sender,
          createdAt: msg.createdAt,
        })),
      });
    }
    return Success(result);
  }
}
