import {MigrationInterface, QueryRunner} from "typeorm";

export class chat1616055845763 implements MigrationInterface {
    name = 'chat1616055845763'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "core"."message" DROP CONSTRAINT "FK_7fbe241c763d2f6ab89b4862471"`);
        await queryRunner.query(`ALTER TABLE "core"."message" RENAME COLUMN "game_chat_id" TO "chat_id"`);
        await queryRunner.query(`CREATE TABLE "core"."chat" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "user_creator_id" uuid NOT NULL, "user_participant_id" uuid NOT NULL, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "PK_d9cacbfe280fd3d517e0dc2a26d" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "core"."message" ADD CONSTRAINT "FK_155b4a9db3a45ee4b7dbcb231bd" FOREIGN KEY ("chat_id") REFERENCES "core"."chat"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "core"."chat" ADD CONSTRAINT "FK_b031dec7df9ca5a40c8dc194a97" FOREIGN KEY ("user_creator_id") REFERENCES "core"."user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "core"."chat" ADD CONSTRAINT "FK_7c87e61b6a1fd4e444213095586" FOREIGN KEY ("user_participant_id") REFERENCES "core"."user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "core"."chat" DROP CONSTRAINT "FK_7c87e61b6a1fd4e444213095586"`);
        await queryRunner.query(`ALTER TABLE "core"."chat" DROP CONSTRAINT "FK_b031dec7df9ca5a40c8dc194a97"`);
        await queryRunner.query(`ALTER TABLE "core"."message" DROP CONSTRAINT "FK_155b4a9db3a45ee4b7dbcb231bd"`);
        await queryRunner.query(`DROP TABLE "core"."chat"`);
        await queryRunner.query(`ALTER TABLE "core"."message" RENAME COLUMN "chat_id" TO "game_chat_id"`);
        await queryRunner.query(`ALTER TABLE "core"."message" ADD CONSTRAINT "FK_7fbe241c763d2f6ab89b4862471" FOREIGN KEY ("game_chat_id") REFERENCES "core"."game_chat"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
