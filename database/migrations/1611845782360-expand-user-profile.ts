import {MigrationInterface, QueryRunner} from "typeorm";

export class expandUserProfile1611845782360 implements MigrationInterface {
    name = 'expandUserProfile1611845782360'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "core"."user_sex_enum" AS ENUM('MALE', 'FEMALE')`);
        await queryRunner.query(`ALTER TABLE "core"."user" ADD "sex" "core"."user_sex_enum"`);
        await queryRunner.query(`CREATE TYPE "core"."user_hand_enum" AS ENUM('RIGHT', 'LEFT')`);
        await queryRunner.query(`ALTER TABLE "core"."user" ADD "hand" "core"."user_hand_enum"`);
        await queryRunner.query(`ALTER TABLE "core"."user" ADD "email" character varying`);
        await queryRunner.query(`ALTER TABLE "core"."user" ADD "year_of_birth" integer`);
        await queryRunner.query(`ALTER TABLE "core"."user" ADD "state" character varying`);
        await queryRunner.query(`ALTER TABLE "core"."user" ADD "playing_style" character varying`);
        await queryRunner.query(`ALTER TABLE "core"."user" ADD "club" character varying`);
        await queryRunner.query(`ALTER TABLE "core"."user" ADD "other_facilities" character varying`);
        await queryRunner.query(`ALTER TABLE "core"."user" ADD "looking_for_tennis_partners_to" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "core"."user" DROP COLUMN "looking_for_tennis_partners_to"`);
        await queryRunner.query(`ALTER TABLE "core"."user" DROP COLUMN "other_facilities"`);
        await queryRunner.query(`ALTER TABLE "core"."user" DROP COLUMN "club"`);
        await queryRunner.query(`ALTER TABLE "core"."user" DROP COLUMN "playing_style"`);
        await queryRunner.query(`ALTER TABLE "core"."user" DROP COLUMN "state"`);
        await queryRunner.query(`ALTER TABLE "core"."user" DROP COLUMN "year_of_birth"`);
        await queryRunner.query(`ALTER TABLE "core"."user" DROP COLUMN "email"`);
        await queryRunner.query(`ALTER TABLE "core"."user" DROP COLUMN "hand"`);
        await queryRunner.query(`DROP TYPE "core"."user_hand_enum"`);
        await queryRunner.query(`ALTER TABLE "core"."user" DROP COLUMN "sex"`);
        await queryRunner.query(`DROP TYPE "core"."user_sex_enum"`);
    }

}
