import { MigrationInterface, QueryRunner } from 'typeorm';

export class addGamePlayerUniqueConstraint1611353488068
  implements MigrationInterface {
  name = 'addGamePlayerUniqueConstraint1611353488068';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "core"."game_player" ADD CONSTRAINT "UQ_aa9b271ed60dbf67fafbc53d87d" UNIQUE ("user_id", "game_id")`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "core"."game_player" DROP CONSTRAINT "UQ_aa9b271ed60dbf67fafbc53d87d"`,
    );
  }
}
