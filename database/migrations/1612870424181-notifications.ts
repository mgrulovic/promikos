import { MigrationInterface, QueryRunner } from 'typeorm';

export class notifications1612870424181 implements MigrationInterface {
  name = 'notifications1612870424181';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TYPE "core"."notification_type_enum" AS ENUM('GAME_INVITATION', 'GAME_ACCEPTANCE', 'GAME_CONFIRMATION', 'GAME_DECLINATION', 'GAME_CANCELATION', 'GAME_WITHDRAWN', 'GAME_CHAT')`,
    );
    await queryRunner.query(
      `CREATE TABLE "core"."notification" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "title" character varying NOT NULL, "body" character varying NOT NULL, "data" jsonb, "expo_push_token" character varying, "type" "core"."notification_type_enum" NOT NULL, "user_id" uuid NOT NULL, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "PK_ff1bf9b263ce7dfccaff46dee95" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "core"."notification" ADD CONSTRAINT "FK_56f14dfccbb95ea3f75003aaff5" FOREIGN KEY ("user_id") REFERENCES "core"."user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "core"."notification" DROP CONSTRAINT "FK_56f14dfccbb95ea3f75003aaff5"`,
    );
    await queryRunner.query(`DROP TABLE "core"."notification"`);
    await queryRunner.query(`DROP TYPE "core"."notification_type_enum"`);
  }
}
