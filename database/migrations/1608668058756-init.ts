import { MigrationInterface, QueryRunner } from 'typeorm';

export class init1608668058756 implements MigrationInterface {
  name = 'init1608668058756';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "core"."access_code" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "phone" character varying NOT NULL, "code" character varying NOT NULL, "redeemed" boolean NOT NULL DEFAULT false, "expired_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW() + INTERVAL '5 minutes', "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "PK_8116fe30a1cc5176e54fea6c802" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TYPE "core"."game_player_status_enum" AS ENUM('CONFIRMED', 'ACCEPTED', 'PENDING', 'DECLINED')`,
    );
    await queryRunner.query(
      `CREATE TYPE "core"."game_player_type_enum" AS ENUM('STUDENT', 'COACH')`,
    );
    await queryRunner.query(
      `CREATE TABLE "core"."game_player" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "status" "core"."game_player_status_enum" NOT NULL DEFAULT 'PENDING', "type" "core"."game_player_type_enum", "user_id" uuid NOT NULL, "game_id" uuid NOT NULL, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "PK_286f76191da65ecc68021deba58" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "core"."message" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "text" text NOT NULL, "sent_at" TIMESTAMP WITH TIME ZONE NOT NULL, "received_at" TIMESTAMP WITH TIME ZONE, "seen_at" TIMESTAMP WITH TIME ZONE, "user_id" uuid NOT NULL, "game_chat_id" uuid NOT NULL, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "PK_60700dee59dc99c6227fa492563" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "core"."game_chat" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "game_id" uuid NOT NULL, "user_creator_id" uuid NOT NULL, "user_participant_id" uuid NOT NULL, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "PK_040bf9d3e34bd053ebf912e4aba" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TYPE "core"."game_game_type_enum" AS ENUM('MATCH', 'TRAINING', 'LESSON', 'TOURNAMENT_PARTNER')`,
    );
    await queryRunner.query(
      `CREATE TABLE "core"."game" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "date" date NOT NULL, "time_from" TIME NOT NULL, "time_to" TIME NOT NULL, "number_of_players" integer NOT NULL, "location" text NOT NULL, "location_change" boolean NOT NULL DEFAULT false, "court_booked" boolean NOT NULL DEFAULT false, "comment" text, "game_type" "core"."game_game_type_enum" NOT NULL DEFAULT 'MATCH', "creator_user_id" uuid NOT NULL, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "PK_33bcc4320910d9a1a6d4ba8e180" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "core"."user" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "expo_push_token" character varying, "first_name" character varying, "last_name" character varying, "phone" character varying NOT NULL, "itn" double precision, "is_setup" boolean NOT NULL DEFAULT false, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "UQ_449b10d30fd8529c4b6af457960" UNIQUE ("phone"), CONSTRAINT "PK_3b382517105438c9eccd2d59f8e" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "core"."favorite_player" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "user_id" uuid NOT NULL, "player_id" uuid NOT NULL, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "PK_3f4d690d4bfa40cbdbff3c58004" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "core"."game_player" ADD CONSTRAINT "FK_e840e723cc1cb1cf07f696ba6da" FOREIGN KEY ("user_id") REFERENCES "core"."user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "core"."game_player" ADD CONSTRAINT "FK_a994f7750c7a8f0628c6b83d259" FOREIGN KEY ("game_id") REFERENCES "core"."game"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "core"."message" ADD CONSTRAINT "FK_69d4571c294ec2cfd2c55adfcf7" FOREIGN KEY ("user_id") REFERENCES "core"."user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "core"."message" ADD CONSTRAINT "FK_7fbe241c763d2f6ab89b4862471" FOREIGN KEY ("game_chat_id") REFERENCES "core"."game_chat"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "core"."game_chat" ADD CONSTRAINT "FK_213ebfa316d2c49cbe08b7bdec5" FOREIGN KEY ("game_id") REFERENCES "core"."game"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "core"."game_chat" ADD CONSTRAINT "FK_bf0f9f2ffe91bc42b115d5e290a" FOREIGN KEY ("user_creator_id") REFERENCES "core"."user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "core"."game_chat" ADD CONSTRAINT "FK_e2a36ab8da531a627282a70915f" FOREIGN KEY ("user_participant_id") REFERENCES "core"."user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "core"."game" ADD CONSTRAINT "FK_8d96b741e2920c6b3f0f148082b" FOREIGN KEY ("creator_user_id") REFERENCES "core"."user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "core"."favorite_player" ADD CONSTRAINT "FK_a3ba13546850213a468cff73903" FOREIGN KEY ("user_id") REFERENCES "core"."user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "core"."favorite_player" ADD CONSTRAINT "FK_29a2c611d259b2e54b2db72a83f" FOREIGN KEY ("player_id") REFERENCES "core"."user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "core"."favorite_player" DROP CONSTRAINT "FK_29a2c611d259b2e54b2db72a83f"`,
    );
    await queryRunner.query(
      `ALTER TABLE "core"."favorite_player" DROP CONSTRAINT "FK_a3ba13546850213a468cff73903"`,
    );
    await queryRunner.query(
      `ALTER TABLE "core"."game" DROP CONSTRAINT "FK_8d96b741e2920c6b3f0f148082b"`,
    );
    await queryRunner.query(
      `ALTER TABLE "core"."game_chat" DROP CONSTRAINT "FK_e2a36ab8da531a627282a70915f"`,
    );
    await queryRunner.query(
      `ALTER TABLE "core"."game_chat" DROP CONSTRAINT "FK_bf0f9f2ffe91bc42b115d5e290a"`,
    );
    await queryRunner.query(
      `ALTER TABLE "core"."game_chat" DROP CONSTRAINT "FK_213ebfa316d2c49cbe08b7bdec5"`,
    );
    await queryRunner.query(
      `ALTER TABLE "core"."message" DROP CONSTRAINT "FK_7fbe241c763d2f6ab89b4862471"`,
    );
    await queryRunner.query(
      `ALTER TABLE "core"."message" DROP CONSTRAINT "FK_69d4571c294ec2cfd2c55adfcf7"`,
    );
    await queryRunner.query(
      `ALTER TABLE "core"."game_player" DROP CONSTRAINT "FK_a994f7750c7a8f0628c6b83d259"`,
    );
    await queryRunner.query(
      `ALTER TABLE "core"."game_player" DROP CONSTRAINT "FK_e840e723cc1cb1cf07f696ba6da"`,
    );
    await queryRunner.query(`DROP TABLE "core"."favorite_player"`);
    await queryRunner.query(`DROP TABLE "core"."user"`);
    await queryRunner.query(`DROP TABLE "core"."game"`);
    await queryRunner.query(`DROP TYPE "core"."game_game_type_enum"`);
    await queryRunner.query(`DROP TABLE "core"."game_chat"`);
    await queryRunner.query(`DROP TABLE "core"."message"`);
    await queryRunner.query(`DROP TABLE "core"."game_player"`);
    await queryRunner.query(`DROP TYPE "core"."game_player_type_enum"`);
    await queryRunner.query(`DROP TYPE "core"."game_player_status_enum"`);
    await queryRunner.query(`DROP TABLE "core"."access_code"`);
  }
}
