import { Repository, EntityRepository } from 'typeorm';
import { User } from '../entities/user.entity';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  async getUsers(id: string, name?: string, setupCompleted?: boolean) {
    const qb = this.createQueryBuilder('users')
      .leftJoinAndSelect('users.favorited', 'favorited')
      .where('users.id != :id', { id })
      .orderBy('first_name', 'ASC');
    if (name) {
      qb.andWhere("first_name || ' ' || last_name ILIKE :name", {
        name: `%${name}%`,
      });
    }
    if (setupCompleted !== undefined) {
      qb.andWhere('is_setup = :setupCompleted', { setupCompleted });
    }
    return qb.getMany();
  }
}
