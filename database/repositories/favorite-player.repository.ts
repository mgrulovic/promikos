import { Repository, EntityRepository } from 'typeorm';
import { FavoritePlayer } from '../entities/favorite-player.entity';

@EntityRepository(FavoritePlayer)
export class FavoritePlayerRepository extends Repository<FavoritePlayer> {
    async getUserFavoritePartners(userId: string) {
        return this.createQueryBuilder('favorite_player')
            .leftJoinAndSelect('favorite_player.player', 'players')
            .where('favorite_player.userId = :userId', { userId })
            .orderBy('players.first_name', 'ASC')
            .getMany();
    }
}
