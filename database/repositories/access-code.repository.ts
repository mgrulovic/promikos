import { Repository, EntityRepository } from 'typeorm';
import { AccessCode } from '../entities/access-code.entity';

@EntityRepository(AccessCode)
export class AccessCodeRepository extends Repository<AccessCode> {}
