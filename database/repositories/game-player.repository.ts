import { Repository, EntityRepository } from 'typeorm';
import { GameInvitationStatus } from '../../application/game/enums/game-invitation-status.enum';
import { GamePlayer } from '../entities/game-player.entity';
import { Game } from '../entities/game.entity';

@EntityRepository(GamePlayer)
export class GamePlayerRepository extends Repository<GamePlayer> {
  async getIsGameFixed(game: Pick<Game, 'id' | 'numberOfPlayers'>) {
    const countOfConfirmedPlayers = await this.count({
      where: {
        gameId: game.id,
        status: GameInvitationStatus.CONFIRMED,
      },
    });
    return countOfConfirmedPlayers === game.numberOfPlayers;
  }

  groupByStatus(gamePlayers: GamePlayer[]) {
    const confirmedInvitations: GamePlayer[] = [];
    const acceptedInvitations: GamePlayer[] = [];
    const pendingInvitations: GamePlayer[] = [];
    const declinedInvitations: GamePlayer[] = [];
    gamePlayers.forEach(invitation => {
      switch (invitation.status) {
        case GameInvitationStatus.CONFIRMED:
          confirmedInvitations.push(invitation);
          break;
        case GameInvitationStatus.ACCEPTED:
          acceptedInvitations.push(invitation);
          break;
        case GameInvitationStatus.PENDING:
          pendingInvitations.push(invitation);
          break;
        case GameInvitationStatus.DECLINED:
          declinedInvitations.push(invitation);
          break;
      }
    });
    return {
      confirmedInvitations,
      acceptedInvitations,
      pendingInvitations,
      declinedInvitations,
    };
  }
}
