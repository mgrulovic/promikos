import { Repository, EntityRepository } from 'typeorm';
import { Game } from '../entities/game.entity';
import { Brackets } from 'typeorm';

@EntityRepository(Game)
export class GameRepository extends Repository<Game> {
  async getAllGames(userId?: string, dateFrom?: string, dateTo?: string) {
    const queryBuilder = this.createQueryBuilder('game').select('game.id');
    if (userId) {
      queryBuilder
        .leftJoin('game.invitations', 'invitation')
        .leftJoin('game.creator', 'creator')
        .leftJoin('invitation.user', 'invitedUser');
    }
    let conn = 'WHERE';
    if (dateFrom) {
      queryBuilder.where('game.date >= :dateFrom', { dateFrom });
      conn = 'AND_WHERE';
    }
    if (dateTo) {
      if (conn === 'WHERE') {
        queryBuilder.where('game.date <= :dateTo', { dateTo });
        conn = 'AND_WHERE';
      } else {
        queryBuilder.andWhere('game.date <= :dateTo', { dateTo });
      }
    }

    if (conn === 'WHERE' && userId) {
      queryBuilder.where(
        new Brackets(queryBuilder =>
          queryBuilder
            .andWhere('invitedUser.id = :userId', { userId })
            .orWhere('game.creatorId=:userId'),
        ),
      );
    } else if (conn === 'AND_WHERE' && userId) {
      queryBuilder.andWhere(
        new Brackets(queryBuilder =>
          queryBuilder
            .andWhere('invitedUser.id = :userId', { userId })
            .orWhere('game.creatorId = :userId'),
        ),
      );
    }
    queryBuilder.orderBy('game.date', 'ASC').addOrderBy('game.timeFrom', 'ASC');
    const gameIds = await queryBuilder.getMany();
    const games = [];
    for (let i = 0; i < gameIds.length; i += 1) {
      const game = await this.getGame(gameIds[i].id);
      games.push(game);
    }
    return games;
  }

  async getGame(gameId: string) {
    return this.createQueryBuilder('game')
      .leftJoinAndSelect('game.invitations', 'invitation')
      .leftJoinAndSelect('game.creator', 'creator')
      .leftJoinAndSelect('invitation.user', 'invitedUser')
      .where('game.id = :gameId', { gameId })
      .getOne();
  }
}
