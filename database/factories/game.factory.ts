import { define } from 'typeorm-seeding';
import * as Faker from 'faker';
import { Game } from '../entities/game.entity';

define(Game, (faker: typeof Faker, context: any) => {
  const game = new Game();
  game.date = context.date;
  game.timeFrom = context.timeFrom;
  game.timeTo = context.timeTo;
  game.numberOfPlayers = context.numberOfPlayers;
  game.location = context.location;
  game.locationChange = context.locationChange;
  game.courtBooked = context.courtBooked;
  game.locationChange = context.locationChange;
  game.comment = context.comment;
  game.gameType = context.gameType;
  game.creatorId = context.creatorId;
  return game;
});
