import { define } from 'typeorm-seeding';
import * as Faker from 'faker';
import { GamePlayer } from '../entities/game-player.entity';

define(GamePlayer, (faker: typeof Faker, context: any) => {
  const gamePlayer = new GamePlayer();
  gamePlayer.userId = context.userId;
  gamePlayer.gameId = context.gameId;
  gamePlayer.status = context.status;
  return gamePlayer;
});
