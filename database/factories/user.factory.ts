import { define } from 'typeorm-seeding';
import * as Faker from 'faker';
import { User } from '../entities/user.entity';

define(User, (faker: typeof Faker, context: any) => {
  const user = new User();
  user.phone = context.phone;
  user.firstName = context.firstName;
  user.lastName = context.lastName;
  user.itn = context.itn;
  user.isSetup = context.isSetup;
  return user;
});
