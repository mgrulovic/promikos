import { ConfigService } from '../config/config.service';

const config = new ConfigService();

export = config.getTypeOrmConfig();
