import {
  Entity,
  PrimaryGeneratedColumn,
  BaseEntity,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
  Column,
  OneToMany,
} from 'typeorm';
import { SCHEMA_CORE } from '../../constants/db.constants';
import { User } from './user.entity';
import { GameType } from '../../application/game/enums/game-type.enum';
import { GamePlayer } from './game-player.entity';
import { parseAndFormat } from '../../core/date-time/formatters';
import {
  DAY_MONTH_FORMAT,
  DB_DATE_FORMAT,
  DB_TIME_FORMAT,
  HOUR_MINUTE_FORMAT,
} from '../../core/date-time/constants';

@Entity('game', { schema: SCHEMA_CORE })
export class Game extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'date', name: 'date' })
  date: string;

  @Column({ type: 'time', name: 'time_from' })
  timeFrom: string | null;

  @Column({ type: 'time', name: 'time_to' })
  timeTo: string | null;

  @Column({ type: 'integer', name: 'number_of_players' })
  numberOfPlayers: number;

  @Column({ type: 'text', name: 'location' })
  location: string;

  @Column({ type: 'boolean', name: 'location_change', default: false })
  locationChange: boolean;

  @Column({ type: 'boolean', name: 'court_booked', default: false })
  courtBooked: boolean;

  @Column({ type: 'text', name: 'comment', nullable: true })
  comment: string | null;

  @Column({
    type: 'enum',
    enum: GameType,
    name: 'game_type',
    default: GameType.MATCH,
  })
  gameType: GameType;

  // relations
  @ManyToOne(
    () => User,
    user => user.games,
  )
  @JoinColumn([{ name: 'creator_user_id', referencedColumnName: 'id' }])
  creator: User;
  @Column({ type: 'uuid', name: 'creator_user_id' })
  creatorId: string;

  @OneToMany(
    () => GamePlayer,
    invitation => invitation.game,
  )
  invitations: GamePlayer[];

  // timestamps
  @CreateDateColumn({
    type: 'timestamp with time zone',
    name: 'created_at',
  })
  createdAt: Date;
  @UpdateDateColumn({
    type: 'timestamp with time zone',
    name: 'updated_at',
  })
  updatedAt: Date;

  // custom domain methods
  parseGameDateAndType() {
    const date = parseAndFormat({
      dateTime: this.date,
      currentFormat: DB_DATE_FORMAT,
      wantedFormat: DAY_MONTH_FORMAT,
    });
    const timeFrom = parseAndFormat({
      dateTime: this.timeFrom,
      currentFormat: DB_TIME_FORMAT,
      wantedFormat: HOUR_MINUTE_FORMAT,
    });
    const timeTo = parseAndFormat({
      dateTime: this.timeTo,
      currentFormat: DB_TIME_FORMAT,
      wantedFormat: HOUR_MINUTE_FORMAT,
    });
    return { date, timeFrom, timeTo };
  }
}
