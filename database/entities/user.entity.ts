import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  BaseEntity,
  OneToMany,
} from 'typeorm';
import { SCHEMA_CORE } from '../../constants/db.constants';
import { Game } from './game.entity';
import { GamePlayer } from './game-player.entity';
import { Message } from './message.entity';
import { Chat } from './chat.entity';
import { FavoritePlayer } from './favorite-player.entity';
import { Sex } from '../../application/user/enums/sex.enum';
import { Hand } from '../../application/user/enums/hand.enum';
import { Notification } from './notification.entity';

@Entity('user', { schema: SCHEMA_CORE })
export class User extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ name: 'expo_push_token', nullable: true })
  expoPushToken: string | null;

  @Column({ name: 'first_name', nullable: true })
  firstName: string | null;

  @Column({ name: 'last_name', nullable: true })
  lastName: string | null;

  @Column({ unique: true })
  phone: string;

  @Column({
    type: 'double precision',
    name: 'itn',
    nullable: true,
  })
  itn: number | null;

  @Column({
    type: 'boolean',
    name: 'is_setup',
    default: false,
  })
  isSetup: boolean;

  @Column({
    type: 'enum',
    enum: Sex,
    name: 'sex',
    nullable: true,
  })
  sex: Sex | null;

  @Column({
    type: 'enum',
    enum: Hand,
    name: 'hand',
    nullable: true,
  })
  hand: Hand | null;

  @Column({ name: 'email', nullable: true })
  email: string | null;

  @Column({ name: 'year_of_birth', nullable: true })
  yearOfBirth: number | null;

  @Column({ name: 'state', nullable: true })
  state: string | null;

  @Column({ name: 'playing_style', nullable: true })
  playingStyle: string | null;

  @Column({ name: 'club', nullable: true })
  club: string | null;

  @Column({ name: 'other_facilities', nullable: true })
  otherFacilities: string | null;

  @Column({ name: 'looking_for_tennis_partners_to', nullable: true })
  lookingForTennisPartnersTo: string | null;

  // relations
  @OneToMany(
    () => Game,
    game => game.creator,
  )
  games: Game[];

  @OneToMany(
    () => GamePlayer,
    invitation => invitation.user,
  )
  invitations: GamePlayer[];

  @OneToMany(
    () => FavoritePlayer,
    favoritePlayer => favoritePlayer.user,
  )
  favorites: FavoritePlayer[];

  @OneToMany(
    () => FavoritePlayer,
    favoritePlayer => favoritePlayer.player,
  )
  favorited: FavoritePlayer[];

  @OneToMany(
    () => Message,
    message => message.sender,
  )
  sentMessages: Message[];

  @OneToMany(
    () => Chat,
    chat => chat.creator,
  )
  createdChats: Chat[];

  @OneToMany(
    () => Chat,
    chat => chat.participant,
  )
  joinedChats: Chat[];

  @OneToMany(
    () => Notification,
    notification => notification.user,
  )
  notifications: Notification[];

  // timestamps
  @CreateDateColumn({
    type: 'timestamp with time zone',
    name: 'created_at',
  })
  createdAt: Date;
  @UpdateDateColumn({
    type: 'timestamp with time zone',
    name: 'updated_at',
  })
  updatedAt: Date;

  // custom domain methods
  getFullName() {
    return `${this.firstName} ${this.lastName}`;
  }
}
