import {
  Column,
  Entity,
  ManyToOne,
  JoinColumn,
  BaseEntity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { User } from './user.entity';
import { Chat } from './chat.entity';
import { SCHEMA_CORE } from '../../constants/db.constants';

@Entity('message', { schema: SCHEMA_CORE })
export class Message extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'text', name: 'text' })
  text: string;

  @Column({ type: 'timestamp with time zone', name: 'sent_at' })
  sentAt: Date;

  @Column({
    type: 'timestamp with time zone',
    name: 'received_at',
    nullable: true,
  })
  receivedAt: Date;

  @Column({ type: 'timestamp with time zone', name: 'seen_at', nullable: true })
  seenAt: Date;

  // relations
  @ManyToOne(
    () => User,
    user => user.sentMessages,
  )
  @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
  sender: User;
  @Column({ type: 'uuid', name: 'user_id' })
  senderId: string;

  @ManyToOne(
    () => Chat,
    chat => chat.messages,
  )
  @JoinColumn([{ name: 'chat_id', referencedColumnName: 'id' }])
  chat: Chat;
  @Column({ type: 'uuid', name: 'chat_id' })
  chatId: string;

  // timestamps
  @CreateDateColumn({
    type: 'timestamp with time zone',
    name: 'created_at',
  })
  createdAt: Date;
  @UpdateDateColumn({
    type: 'timestamp with time zone',
    name: 'updated_at',
  })
  updatedAt: Date;
}
