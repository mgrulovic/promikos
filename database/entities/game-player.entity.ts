import {
  Entity,
  PrimaryGeneratedColumn,
  BaseEntity,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
  Column,
  Unique,
} from 'typeorm';
import { SCHEMA_CORE } from '../../constants/db.constants';
import { User } from './user.entity';
import { GameInvitationStatus } from '../../application/game/enums/game-invitation-status.enum';
import { Game } from './game.entity';
import { GamePlayerType } from '../../application/game/enums/game-player-type.enum';

@Entity('game_player', { schema: SCHEMA_CORE })
@Unique(['userId', 'gameId'])
export class GamePlayer extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    type: 'enum',
    enum: GameInvitationStatus,
    name: 'status',
    default: GameInvitationStatus.PENDING,
  })
  status: GameInvitationStatus;

  @Column({
    type: 'enum',
    enum: GamePlayerType,
    name: 'type',
    nullable: true,
  })
  type: GamePlayerType;

  // relations
  @ManyToOne(
    () => User,
    user => user.invitations,
  )
  @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
  user: User;
  @Column({ type: 'uuid', name: 'user_id' })
  userId: string;

  @ManyToOne(
    () => Game,
    game => game.invitations,
  )
  @JoinColumn([{ name: 'game_id', referencedColumnName: 'id' }])
  game: Game;
  @Column({ type: 'uuid', name: 'game_id' })
  gameId: string;

  // timestamps
  @CreateDateColumn({
    type: 'timestamp with time zone',
    name: 'created_at',
  })
  createdAt: Date;
  @UpdateDateColumn({
    type: 'timestamp with time zone',
    name: 'updated_at',
  })
  updatedAt: Date;
}
