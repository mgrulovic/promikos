import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { SCHEMA_CORE } from '../../constants/db.constants';
import {
  IChatNotificationData,
  INotificationData,
} from '../../notification/interface/notification-data.interface';
import { NOTIFICATION_TYPE } from '../../notification/notification-type.enum';
import { User } from './user.entity';

@Entity('notification', { schema: SCHEMA_CORE })
export class Notification extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  title: string;

  @Column()
  body: string;

  @Column({ type: 'jsonb', nullable: true })
  data?: IChatNotificationData | INotificationData;

  @Column({ name: 'expo_push_token', nullable: true })
  expoPushToken: string | null;

  @Column({
    type: 'enum',
    enum: NOTIFICATION_TYPE,
    name: 'type',
  })
  type: NOTIFICATION_TYPE;

  // relations
  @ManyToOne(
    () => User,
    user => user.notifications,
  )
  @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
  user: User;
  @Column({ type: 'uuid', name: 'user_id' })
  userId: string;

  // timestamps
  @CreateDateColumn({
    type: 'timestamp with time zone',
    name: 'created_at',
  })
  createdAt: Date;
}
