import {
  Entity,
  PrimaryGeneratedColumn,
  BaseEntity,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
  Column,
} from 'typeorm';
import { SCHEMA_CORE } from '../../constants/db.constants';
import { User } from './user.entity';

@Entity('favorite_player', { schema: SCHEMA_CORE })
export class FavoritePlayer extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  // relations
  @ManyToOne(
    () => User,
    user => user.favorites,
  )
  @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
  user: User;

  @Column({ type: 'uuid', name: 'user_id' })
  userId: string;

  @ManyToOne(
    () => User,
    player => player.favorited,
  )
  @JoinColumn([{ name: 'player_id', referencedColumnName: 'id' }])
  player: User;

  @Column({ type: 'uuid', name: 'player_id' })
  playerId: string;

  // timestamps
  @CreateDateColumn({
    type: 'timestamp with time zone',
    name: 'created_at',
  })
  createdAt: Date;
  @UpdateDateColumn({
    type: 'timestamp with time zone',
    name: 'updated_at',
  })
  updatedAt: Date;
}
