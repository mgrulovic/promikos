import {
  Column,
  Entity,
  ManyToOne,
  JoinColumn,
  OneToMany,
  BaseEntity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { User } from './user.entity';
import { Message } from './message.entity';
import { SCHEMA_CORE } from '../../constants/db.constants';

@Entity('chat', { schema: SCHEMA_CORE })
export class Chat extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  // relations
  @OneToMany(
    () => Message,
    message => message.chat,
  )
  messages: Message[];

  @ManyToOne(
    () => User,
    user => user.createdChats,
  )
  @JoinColumn([{ name: 'user_creator_id', referencedColumnName: 'id' }])
  creator: User;
  @Column({ type: 'uuid', name: 'user_creator_id' })
  creatorId: string;

  @ManyToOne(
    () => User,
    user => user.joinedChats,
  )
  @JoinColumn([{ name: 'user_participant_id', referencedColumnName: 'id' }])
  participant: User;
  @Column({ type: 'uuid', name: 'user_participant_id' })
  participantId: string;

  // timestamps
  @CreateDateColumn({
    type: 'timestamp with time zone',
    name: 'created_at',
  })
  createdAt: Date;
  @UpdateDateColumn({
    type: 'timestamp with time zone',
    name: 'updated_at',
  })
  updatedAt: Date;
}
