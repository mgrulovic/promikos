import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  BaseEntity,
} from 'typeorm';
import { SCHEMA_CORE } from '../../constants/db.constants';

@Entity('access_code', { schema: SCHEMA_CORE })
export class AccessCode extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  phone: string;

  @Column()
  code: string;

  @Column({ type: 'boolean', default: false })
  redeemed: boolean;

  @Column({
    name: 'expired_at',
    type: 'timestamp with time zone',
    default: () => `NOW() + INTERVAL '5 minutes'`,
  })
  expiredAt: Date;

  // timestamps
  @CreateDateColumn({
    type: 'timestamp with time zone',
    name: 'created_at',
  })
  createdAt: Date;
  @UpdateDateColumn({
    type: 'timestamp with time zone',
    name: 'updated_at',
  })
  updatedAt: Date;
}
